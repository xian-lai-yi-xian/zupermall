/*
 Navicat Premium Data Transfer

 Source Server         : reggie
 Source Server Type    : MySQL
 Source Server Version : 80013
 Source Host           : localhost:3306
 Source Schema         : reggie

 Target Server Type    : MySQL
 Target Server Version : 80013
 File Encoding         : 65001

 Date: 22/09/2022 23:51:42
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;



-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category`  (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `type` int(11) NULL DEFAULT NULL COMMENT '类型   1 商品分类 2 套餐分类',
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '分类名称',
  `sort` int(11) NOT NULL DEFAULT 0 COMMENT '顺序',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  `create_user` bigint(20) NOT NULL COMMENT '创建人',
  `update_user` bigint(20) NOT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `idx_category_name`(`name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '商品分类' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES (1572836931410051074, 1, '手机', 1, '2022-09-22 14:35:21', '2022-09-22 15:34:25', 1, 1);
INSERT INTO `category` VALUES (1572836948669612033, 1, '衣服', 1, '2022-09-22 14:35:25', '2022-09-22 14:35:25', 1, 1);
INSERT INTO `category` VALUES (1572836987471118338, 1, '零食', 1, '2022-09-22 14:35:34', '2022-09-22 14:35:34', 1, 1);
INSERT INTO `category` VALUES (1572837137258102785, 1, '电脑', 2, '2022-09-22 14:36:10', '2022-09-22 14:36:10', 1, 1);
INSERT INTO `category` VALUES (1572837195512791041, 1, '鞋类', 2, '2022-09-22 14:36:24', '2022-09-22 14:36:24', 1, 1);
INSERT INTO `category` VALUES (1572851511259545602, 1, '日用品', 3, '2022-09-22 15:33:17', '2022-09-22 15:33:17', 1, 1);
INSERT INTO `category` VALUES (1572871074986467330, 1, '家电', 2, '2022-09-22 16:51:01', '2022-09-22 16:51:01', 1, 1);

-- ----------------------------
-- Table structure for employee
-- ----------------------------
DROP TABLE IF EXISTS `employee`;
CREATE TABLE `employee`  (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `name` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '姓名',
  `username` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '用户名',
  `password` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '密码',
  `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '手机号',
  `sex` varchar(2) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '性别',
  `id_number` varchar(18) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '身份证号',
  `status` int(11) NOT NULL DEFAULT 1 COMMENT '状态 0:禁用，1:正常',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  `create_user` bigint(20) NOT NULL COMMENT '创建人',
  `update_user` bigint(20) NOT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `idx_username`(`username`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '员工信息' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of employee
-- ----------------------------
INSERT INTO `employee` VALUES (1, '管理员', 'admin', 'e10adc3949ba59abbe56e057f20f883e', '13812312312', '1', '110101199001010047', 1, '2021-05-06 17:20:07', '2021-05-10 02:24:09', 1, 1);
INSERT INTO `employee` VALUES (1553598208658137089, '张三', 'zhangsan', 'e10adc3949ba59abbe56e057f20f883e', '13274628391', '1', '123456123456123456', 1, '2022-07-31 12:27:32', '2022-07-31 12:27:32', 1, 1);
INSERT INTO `employee` VALUES (1553930692348350465, '李四', 'lisi', 'e10adc3949ba59abbe56e057f20f883e', '13455364777', '1', '123456123456123456', 1, '2022-08-01 10:28:42', '2022-08-01 10:28:42', 1, 1);
INSERT INTO `employee` VALUES (1553956049902411778, 'test01', 'test01', 'e10adc3949ba59abbe56e057f20f883e', '13456789022', '1', '111222333444555666', 1, '2022-08-01 12:09:28', '2022-08-01 12:09:28', 1, 1);
INSERT INTO `employee` VALUES (1572772686806847489, '虎林', 'hulin', 'e10adc3949ba59abbe56e057f20f883e', '13889074582', '1', '619078200109094010', 1, '2022-09-22 10:20:04', '2022-09-22 15:13:35', 1, 1);



-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders`  (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `number` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '订单号',
  `status` int(11) NOT NULL DEFAULT 1 COMMENT '订单状态 1待付款，2待派送，3已派送，4已完成，5已取消',
  `user_id` bigint(20) NOT NULL COMMENT '下单用户',
  `address_book_id` bigint(20) NOT NULL COMMENT '地址id',
  `order_time` datetime NOT NULL COMMENT '下单时间',
  `checkout_time` datetime NOT NULL COMMENT '结账时间',
  `pay_method` int(11) NOT NULL DEFAULT 1 COMMENT '支付方式 1微信,2支付宝',
  `amount` decimal(10, 2) NOT NULL COMMENT '实收金额',
  `remark` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注',
  `phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `consignee` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '订单表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of orders
-- ----------------------------
INSERT INTO `orders` VALUES (272721661, '272721661', 4, 42343242, 24234234234, '2022-09-21 14:33:49', '2022-09-21 14:33:52', 1, 322.00, NULL, '18765342533', '北京市朝阳区', NULL, '王先生');
INSERT INTO `orders` VALUES (1556479319528722433, '1556479319528722433', 4, 1556158942088814593, 1556479260363870209, '2022-08-08 11:16:02', '2022-08-08 11:16:02', 1, 90.00, '', '18392531675', '陕西西安雁塔区', NULL, '康');
INSERT INTO `orders` VALUES (1556483440696676353, '1556483440696676353', 4, 1556158942088814593, 1556479260363870209, '2022-08-08 11:32:25', '2022-08-08 11:32:25', 1, 83.00, '', '18392531675', '陕西西安雁塔区', NULL, '康');
INSERT INTO `orders` VALUES (1556604585399918593, '1556604585399918593', 4, 1556158942088814593, 1556479260363870209, '2022-08-08 19:33:48', '2022-08-08 19:33:48', 1, 83.00, '', '18392531675', '陕西西安雁塔区', NULL, '康');

-- ----------------------------
-- Table structure for setmeal
-- ----------------------------
DROP TABLE IF EXISTS `setmeal`;
CREATE TABLE `setmeal`  (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `category_id` bigint(20) NOT NULL COMMENT '商品分类id',
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '套餐名称',
  `price` decimal(10, 2) NOT NULL COMMENT '套餐价格',
  `status` int(11) NULL DEFAULT NULL COMMENT '状态 0:停用 1:启用',
  `code` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '编码',
  `description` varchar(512) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '描述信息',
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '图片',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  `create_user` bigint(20) NOT NULL COMMENT '创建人',
  `update_user` bigint(20) NOT NULL COMMENT '修改人',
  `is_deleted` int(11) NOT NULL DEFAULT 0 COMMENT '是否删除',
  `count` int(11) NOT NULL COMMENT '数量',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `idx_setmeal_name`(`name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '套餐' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of setmeal
-- ----------------------------
INSERT INTO `setmeal` VALUES (1572840636654010369, 1572836931410051074, '华为Mate 40Pro', 549900.00, 0, '', '华为mate40 Pro 新品5G手机 秘银色8G+256G 5G版全网通', '7fec4e8f-c64f-4caa-b3b4-813f04868d5a.png', '2022-09-22 14:50:04', '2022-09-22 15:47:51', 1, 1, 0, 200);
INSERT INTO `setmeal` VALUES (1572844138969153538, 1572836948669612033, '2022新款毛衣裙收腰针织连衣裙配大衣内', 21900.00, 0, '', '', '6bc567b9-d33a-49be-b6d6-a5938d31ae19.png', '2022-09-22 15:03:59', '2022-09-22 15:47:51', 1, 1, 0, 500);
INSERT INTO `setmeal` VALUES (1572844685772177409, 1572836987471118338, '【良品铺子-大罐快乐每日坚果500g】罐', 5200.00, 0, '', '', '0414e6b1-fa43-4136-a312-caf5d9e6c61a.png', '2022-09-22 15:06:10', '2022-09-22 15:47:51', 1, 1, 0, 533);
INSERT INTO `setmeal` VALUES (1572845420408717313, 1572837195512791041, '李宁童鞋儿童篮球鞋男童韦德全城8青少年减', 36800.00, 0, '', '', '7868e99e-65da-413f-9c17-07ac70078e97.png', '2022-09-22 15:09:05', '2022-09-22 15:47:51', 1, 1, 0, 654);
INSERT INTO `setmeal` VALUES (1572846195486736386, 1572837137258102785, '华硕ROG玩家国度 幻14', 758900.00, 0, '', '', '39b98b38-025d-4bc8-a7c7-45a0bd9f3d80.png', '2022-09-22 15:12:10', '2022-09-22 15:47:51', 1, 1, 0, 45);
INSERT INTO `setmeal` VALUES (1572883879659708418, 1572851511259545602, '洗衣液', 2000.00, 1, '', '', 'be406062-564d-4ca5-ba4b-b93affd6f471.png', '2022-09-22 17:41:54', '2022-09-22 17:42:05', 1, 1, 0, 313);




